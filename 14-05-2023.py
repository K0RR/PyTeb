import operator
from tkinter import *
from tkinter.ttk import *
okno = Tk()
okno.geometry('320x140')
okno.config(borderwidth=8)
okno.option_add('*Font', '20')
etykieta = Label()
etykieta.grid(row=0, column=0)
liczba_text = ""
liczba1 = 0
liczba2 = 0
dzialanie = ""
cyfra = 0
znaki = {
    '+' : operator.add,
    '-' : operator.sub,
    '*' : operator.mul,
    '/' : operator.truediv, 
} 

def wynik():
    global liczba_text
    global liczba1
    global dzialanie
    liczba2 = int(liczba_text)
    etykieta.config(text=znaki[dzialanie](liczba1,liczba2))

def kropka(): #TODO nie działa
    global liczba_text
    # global liczba1
    # liczba1 = int(liczba_text)
    etykieta.config(text=liczba_text+".")

def dodaj():
    global liczba_text
    global liczba1
    global dzialanie
    liczba1 = int(liczba_text)
    liczba_text = ""
    dzialanie = "+"
    etykieta.config(text="")

#     def test():
#         print("test")
        
# dodaj.test()

def odejmij():
    global liczba_text
    global liczba1
    global dzialanie
    liczba1 = int(liczba_text)
    liczba_text = ""
    dzialanie = "-"
    etykieta.config(text="")

def podziel():
    global liczba_text
    global liczba1
    global dzialanie
    liczba1 = int(liczba_text)
    liczba_text = ""
    dzialanie = "/"
    etykieta.config(text="")

def pomnoz():
    global liczba_text
    global liczba1
    global dzialanie
    liczba1 = int(liczba_text)
    liczba_text = ""
    dzialanie = "*"
    etykieta.config(text="")

def dodaj0():
    global liczba_text
    liczba_text += "0"
    etykieta.config(text=liczba_text)
    
def dodaj1():
    global liczba_text
    liczba_text += "1"
    etykieta.config(text=liczba_text)

def dodaj2():
    global liczba_text
    liczba_text += "2"
    etykieta.config(text=liczba_text)

def dodaj3():
    global liczba_text
    liczba_text += "3"
    etykieta.config(text=liczba_text)

def dodaj4():
    global liczba_text
    liczba_text += "4"
    etykieta.config(text=liczba_text)

def dodaj5():
    global liczba_text
    liczba_text += "5"
    etykieta.config(text=liczba_text)

def dodaj6():
    global liczba_text
    liczba_text += "6"
    etykieta.config(text=liczba_text)

def dodaj7():
    global liczba_text
    liczba_text += "7"
    etykieta.config(text=liczba_text)

def dodaj8():
    global liczba_text
    liczba_text += "8"
    etykieta.config(text=liczba_text)

def dodaj9():
    global liczba_text
    liczba_text += "9"
    etykieta.config(text=liczba_text)

# for i in range(10):
#     def dod0

# def licznik():
#     global cyfra += 1

# while cyfra != 9:
#     def cyfry():
#         #TODO
#     print("dokonczyc")

przycisk0 = Button(text="0", command=lambda: [dodaj0(), cyfra := cyfra + 1])
przycisk0.grid(row=4, column=1)

przycisk1 = Button(text="1", command=lambda: [dodaj1(), cyfra := cyfra + 1])
przycisk1.grid(row=1, column=0)
print(cyfra)
przycisk2 = Button(text="2", command=dodaj2)
przycisk2.grid(row=1, column=1)

przycisk3 = Button(text="3", command=dodaj3)
przycisk3.grid(row=1, column=2)

przycisk4 = Button(text="4", command=dodaj4)
przycisk4.grid(row=2, column=0)

przycisk5 = Button(text="5", command=dodaj5)
przycisk5.grid(row=2, column=1)

przycisk6 = Button(text="6", command=dodaj6)
przycisk6.grid(row=2, column=2)

przycisk7 = Button(text="7", command=dodaj7)
przycisk7.grid(row=3, column=0)

przycisk8 = Button(text="8", command=dodaj8)
przycisk8.grid(row=3, column=1)

przycisk9 = Button(text="9", command=dodaj9)
przycisk9.grid(row=3, column=2)

przycisk_dodaj = Button(text="+", command=dodaj)
przycisk_dodaj.grid(row=1, column=3)

przycisk_odejmij = Button(text="-", command=odejmij)
przycisk_odejmij.grid(row=2, column=3)

przycisk_podziel = Button(text="/", command=podziel)
przycisk_podziel.grid(row=3, column=3)

przycisk_pomnoz = Button(text="*", command=pomnoz)
przycisk_pomnoz.grid(row=4, column=3)

przycisk_wynik = Button(text="=", command=wynik)
przycisk_wynik.grid(row=4, column=2)

przycisk_kropka = Button(text=".", command=kropka)
przycisk_kropka.grid(row=4, column=0)

okno.mainloop()