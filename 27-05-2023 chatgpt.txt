from tkinter import *

window = Tk()

wybor = IntVar()

window.geometry("400x300")
liczba1 = Entry()
liczba2 = Entry()
liczba1.grid(row=0, column=0)
liczba2.grid(row=1, column=0)
e_liczba1 = Label(text="liczba 1")
e_liczba2 = Label(text="liczba 2")
e_liczba1.grid(row=0, column=1)
e_liczba2.grid(row=1, column=1)

radio = Radiobutton(text='rosnaco', variable=wybor, value=1)
radio.grid(row=2, column=0)
radio = Radiobutton(text='malejaco', variable=wybor, value=-1)
radio.grid(row=3, column=0)

przycisk = Button(text="generuj")
przycisk.grid(row=4, column=0)

lista_var = StringVar()
etykieta = Label(textvariable=lista_var)
etykieta.grid(row=5, column=0)

def radio_klik():
    liczba1_value = int(liczba1.get())
    liczba2_value = int(liczba2.get())

    mniejsza, wieksza = min(liczba1_value, liczba2_value), max(liczba1_value, liczba2_value)
    
    if wybor.get() == 1:
        kierunek = 1
        pierwsza = mniejsza
        druga = wieksza
    else:
        kierunek = -1
        pierwsza = wieksza
        druga = mniejsza

    lista = [i for i in range(pierwsza, druga, kierunek)]
    lista_var.set(lista)

przycisk.config(command=radio_klik)

window.mainloop()
