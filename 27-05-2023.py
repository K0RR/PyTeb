from tkinter import *
from tkinter import filedialog

window = Tk()

lista = []
etykieta = Label(text=lista)
etykieta.grid(row=5, column=0)


def radio_klik():
    lista.clear()
    liczba1_value = int(liczba1.get())
    liczba2_value = int(liczba2.get())
    if liczba1_value > liczba2_value:
        mniejsza = liczba2_value
        wieksza = liczba1_value
    else:
        wieksza = liczba2_value
        mniejsza = liczba1_value

    if wybor.get() == 1:
        kierunek = 1
        pierwsza = mniejsza
        druga = wieksza + 1
    else:
        kierunek = -1
        pierwsza = wieksza
        druga = mniejsza - 1

    for i in range(pierwsza, druga, kierunek):
        lista.append(i)
        etykieta.config(text=lista)


def zapisz():
    nazwa_pliku = filedialog.asksaveasfilename(filetypes=[("Dowolny plik", "*.txt")])
    plik = open(nazwa_pliku, "w")
    plik.write(', '.join(map(str, lista)))
    plik.close()


wybor = IntVar()

window.geometry("400x300")

liczba1_entry = Entry(window)
liczba1_entry.grid(row=0, column=0)

liczba2_entry = Entry(window)
liczba2_entry.grid(row=1, column=0)

e_liczba1 = Label(text="liczba 1").grid(row=0, column=1)
e_liczba2 = Label(text="liczba 2").grid(row=1, column=1)

radio1 = Radiobutton(text='rosnaco', variable=wybor, value=1).grid(row=2, column=0)
radio2 = Radiobutton(text='malejaco', variable=wybor, value=-1).grid(row=3, column=0)

przycisk = Button(text="generuj", command=radio_klik).grid(row=4, column=0)
zapis = Button(text="zapisz do pliku", command=zapisz).grid(row=4, column=1)

liczba1 = liczba1_entry
liczba2 = liczba2_entry

window.mainloop()
