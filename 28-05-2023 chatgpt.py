from tkinter import *

okno = Tk()
okno.geometry('400x400')

lista = []
ceny = []

d_produkt = Entry(okno)
d_produkt.grid(row=0, column=0)


def Dodaj():
    lista.append(d_produkt.get())
    ceny.append(cena.get())
    d_produkt.delete(0, END)
    cena.delete(0, END)
    Lista()


def Kup():
    if (wybrane := l_produkt.curselection()):
        del lista[wybrane[0]]
        del ceny[wybrane[0]]
        Lista()


def Lista():
    l_produkt.delete(0, END)
    for i in range(len(lista)):
        l_produkt.insert(END, f"{i + 1}. {lista[i]} - {ceny[i]}")


d_produkt_b = Button(text="Dodaj", command=Dodaj)
d_produkt_b.grid(row=0, column=1)

l_produkt = Listbox()
l_produkt.grid(row=1, column=0)

k_produkt = Button(text="Kup", command=Kup)
k_produkt.grid(row=2, column=1)

cena = Entry(okno)
cena.grid(row=1, column=1)


def Paragon():
    podokno = Toplevel()
    podokno.geometry('400x400')
    paragon_l = Label(podokno, text="Paragon:")
    paragon_l.pack()
    combined_data = dict(zip(lista, ceny))  # Combine lista and ceny into a dictionary
    for item, price in combined_data.items():
        if item and price:
            item_l = Label(podokno, text=f"{item} - {price}")
            item_l.pack()


paragon = Button(text="Zapłać", command=Paragon)
paragon.grid(row=3, column=1)

okno.mainloop()
