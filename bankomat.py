from tkinter import *
import mysql.connector

import bankomat_menu

def zaloguj():
    global mycursor, pin, fetch
    mydb = mysql.connector.connect(host="localhost", user='root', password='', database="python")
    mycursor = mydb.cursor()

    mycursor.execute("SELECT * FROM klienci")
    komunikat = Label(okno, text='PIN zawiera znaki inne niż cyfry')
    fetch = mycursor.fetchall()
    for x in fetch:
        try:
            if len(wprowadz_pin.get()) != 4:
                komunikat.config(text="PIN jest za krótki")
                raise Exception
            if int(wprowadz_pin.get()) == x[1]:
                pin = int(wprowadz_pin.get())
                okno.destroy()
                menu()
                break
            else:
                komunikat.config(text="PIN nie istnieje")
                raise Exception
        except Exception:
            komunikat.grid(row=2, column=1)
    # TODO przebudować z użyciem while i uprościć


def menu():
    global saldo
    okno = Tk()
    okno.geometry('280x130')
    for x in fetch:
        if pin == x[1]:
            saldo = x[2]
    napis_saldo = Label(okno, text="Twoje saldo: " + str(saldo), font=('Arial', 20))
    napis_saldo.grid(row=0, column=0, padx=5, pady=5, columnspan=3)
    przycisk_his = Button(okno, text='Historia', command=lambda: bankomat_menu.historia(pin))
    przycisk_his.grid(row=1, column=0)
    przycisk_wplata = Button(okno, text='Wpłać', command=lambda: bankomat_menu.wplac(pin))
    przycisk_wplata.grid(row=1, column=1)
    przycisk_wyplata = Button(okno, text='Wypłać', command=lambda: bankomat_menu.wyplac(pin))
    przycisk_wyplata.grid(row=1, column=2)


okno = Tk()
okno.geometry('220x90')
napis_pin = Label(okno, text='PIN:', padx=5, font=('Arial', 10))
napis_pin.grid(row=0, column=0)
wprowadz_pin = Entry(okno)
wprowadz_pin.grid(row=0, column=1)
przycisk_zaloguj = Button(okno, text='Zaloguj', command=zaloguj)
przycisk_zaloguj.grid(row=2, column=0)

okno.mainloop()
