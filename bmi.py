from tkinter import *

okno = Tk()
def funkcja():
    try:
        wzrost_pot = float(wzrost.get())**2
        licz = int(waga.get())/wzrost_pot
        wynik = Label(text=round(licz, 2))
        wynik.grid(row=4, column=2)
        
        if licz > 18 and licz < 25:
            etykieta_wynik = Label(text="zdrowy")
            etykieta_wynik.grid(row=4,column=50)
        elif licz > 25:
            etykieta_wynik = Label(text="za dużo")
            etykieta_wynik.grid(row=4,column=50)
        else:
            etykieta_wynik = Label(text="za mało")
            etykieta_wynik.grid(row=4,column=50)
    except:
        blad = Label(text="coś poszło nie tak")
        blad.grid(row=5,column=50)


# pseudo kod dla odstępu
padding = Label()
padding.grid(row=0,column=0)
waga = Entry(width=5)
waga.grid(row=1,column=1)
etykieta_waga = Label(text="waga w kilogramach")
etykieta_waga.grid(row=1,column=2)
wzrost = Entry(width=5)
wzrost.grid(row=2,column=1)
etykieta_wzrost = Label(text="wzrost w metrach (rozdziel '.')")
etykieta_wzrost.grid(row=2,column=2)
przycisk = Button(text='policz', command=funkcja)
okno.bind('<Return>', funkcja)
przycisk.grid(row=4,column=1)

okno.mainloop()