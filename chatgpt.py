from tkinter import *

okno = Tk()
okno.geometry('320x320')

# create a Label widget and place it on the grid
etykieta = Label(okno, text="")
etykieta.grid(row=0, column=0, columnspan=4)

# define the functions for button clicks
def dodaj1():
    global liczba_text
    liczba_text += "1"
    etykieta.config(text=liczba_text)

def dodaj2():
    global liczba_text
    liczba_text += "2"
    etykieta.config(text=liczba_text)

def dodaj3():
    global liczba_text
    liczba_text += "3"
    etykieta.config(text=liczba_text)

def dodaj():
    global liczba_text
    global liczba1
    global dzialanie
    liczba1 = int(liczba_text)
    liczba_text = ""
    dzialanie = 1

def wynik():
    global liczba_text
    global liczba1
    if dzialanie == 1:
        wynik = liczba1 + int(liczba_text)
    etykieta.config(text=wynik)

# create Button widgets and place them on the grid
przycisk1 = Button(okno, text="1", command=dodaj1)
przycisk1.grid(row=1, column=0)

przycisk2 = Button(okno, text="2", command=dodaj2)
przycisk2.grid(row=1, column=1)

przycisk3 = Button(okno, text="3", command=dodaj3)
przycisk3.grid(row=1, column=2)

przycisk_dodaj = Button(okno, text="+", command=dodaj)
przycisk_dodaj.grid(row=1, column=3)

przycisk_wynik = Button(okno, text="=", command=wynik)
przycisk_wynik.grid(row=2, column=3)

# initialize some variables
liczba_text = ""
liczba1 = 0
dzialanie = 0

okno.mainloop()
