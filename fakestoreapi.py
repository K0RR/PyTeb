import requests
from tkinter import *

okno = Tk()
okno.geometry('0x200')
response = requests.get("https://fakestoreapi.com/users").json()


def tele():
    plik = open('telefony.txt', 'w')
    for el in response:
        plik.write(el['phone'] + '\n')
    plik.close()


def email():
    plik = open('emaile.txt', 'w')
    for el in response:
        plik.write(el['email'] + '\n')
    plik.close()


przycisk_e = Button(text='E-mail', command=email)
przycisk_e.grid(row=0, column=0)
przycisk_t = Button(text='Telefon', command=tele)
przycisk_t.grid(row=1, column=0)

okno.mainloop()
