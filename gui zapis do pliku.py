from tkinter import *
# from tkinter.ttk import *
okno = Tk()
sortowanko = []
okno.geometry('500x500')
okno.config(borderwidth=8)
okno.option_add('*Font', '20')
def wyczysc():
    p_tekstowe.delete("1.0", END)
def dopisz():
    plik = open("plik.txt", "a")
    plik.write(p_tekstowe.get("1.0", END))
    plik.close()
    p_tekstowe.delete("1.0", END)
def czytaj():
    p_tekstowe.delete("1.0", END)
    try:
        plik = open("plik.txt", "r")
        for i in plik:
            p_tekstowe.insert(END, i)
        plik.close()
    except:
        p_tekstowe.insert(END, "BRAK PLIKU")
def sortuj():
    for i in p_tekstowe.get("1.0", END):
        sortowanko.append(i)
    print(sortowanko)
    sortowanko.sort()
    # print(sortowanko)
    p_tekstowe.delete("1.0", END)
    p_tekstowe.insert(END, sortowanko)
def zastap():
    plik = open("plik.txt", "w")
    plik.write(p_tekstowe.get("1.0",END))
    plik.close()

p_tekstowe = Text(width=15, height=4)
p_tekstowe.grid(row=0, column=0)
czysc = Button(text="Wyczyść", command=wyczysc)
czysc.grid(row=0, column=1)
dodaj = Button(text="Dodaj", command=dopisz)
dodaj.grid(row=1, column=1)
pokaz = Button(text="Pokaż", command=czytaj)
pokaz.grid(row=2, column=1)
nadpisz = Button(text="Zastąp", command=zastap)
nadpisz.grid(row=3, column=1)
sortuj = Button(text="Posortuj", command=sortuj)
sortuj.grid(row=4, column=1)

okno.mainloop()