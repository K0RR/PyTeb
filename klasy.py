# class osoba:
#     def __init__(self,imie,nazwisko,wiek):
#         self.imie = imie
#         self.nazwisko = nazwisko
#         self.__wiek = wiek
#     @property
#     def wiek(self):
#         return self.__wiek
#     @wiek.setter
#     def wiek(self,wiek):
#         if wiek < 10:
#             self.__wiek = 10
#         else:
#             self.__wiek = wiek
#     def rok_urodzenia(self):
#         return 2023 - self.__wiek
# osoba1 = osoba("janek","kowalski",16)
# osoba1.wiek = -22
# print(osoba1.wiek)
# print(osoba1.rok_urodzenia())



# class prostokat:
#     def __init__(self,dlugosc,szerokosc):
#         self.dlugosc = dlugosc
#         self.szerokosc = szerokosc
        
#     def get_dlugosc(self):
#         return self.dlugosc
        
#     def set_dlugosc(self, dlugosc):
#         self.dlugosc = dlugosc
            
#     def get_szerokosc(self):
#         return self.szerokosc
        
#     def set_szerokosc(self, szerokosc):
#         self.szerokosc = szerokosc
            
#     def pole(self):
#         return self.dlugosc * self.szerokosc
        
#     def obwod(self):
#         return 2 * (self.dlugosc + self.szerokosc)
        
#     def przekatna(self):
#         return (self.dlugosc ** 2 + self.szerokosc ** 2) ** 0.5
    
#     @property
#     def szerokosc(self):
#         return self.__szerokosc
#     @szerokosc.setter
#     def szerokosc(self,szerokosc):
#         if szerokosc <= 0:
#             self.__szerokosc = 1
#         else:
#             self.__szerokosc = szerokosc

#     @property
#     def dlugosc(self):
#         return self.__dlugosc
#     @dlugosc.setter
#     def dlugosc(self,dlugosc):
#         if dlugosc <= 0:
#             self.__dlugosc = 1
#         else:
#             self.__dlugosc = dlugosc

# prostokat = prostokat(5, 3)
# print(f"Długość: {prostokat.get_dlugosc()}, Szerokość: {prostokat.get_szerokosc()}")

# prostokat.set_dlugosc(10)
# prostokat.set_szerokosc(6)
# print(f"Długość: {prostokat.get_dlugosc()}, Szerokość: {prostokat.get_szerokosc()}")
# print(f"Pole: {prostokat.pole()}")
# print(f"Obwód: {prostokat.obwod()}")
# print(f"Długość przekątnej: {prostokat.przekatna()}")



# class disc:
#     def __init__(self,model,capacity,price):
#         self.__model = model
#         self.__capacity = capacity
#         self.__price = price
#         self.__vat = 23

#     @property
#     def model(self):
#         return self.__model
#     @model.setter    
#     def set_model(self, model):
#         self.model = model
#     @property
#     def capacity(self):
#         return self.__capacity
#     @capacity.setter    
#     def set_capacity(self, capacity):
#         self.capacity = capacity
#     @property
#     def price(self):
#         return self.__price
#     @price.setter
#     def set_price(self, price):
#         self.price = price
#     def vat(self,vat):
#         self.vat = vat

#     def info(self):
#         return "model: " + self.__model + ' ' + "pojemnosc: " + str(self.__capacity)
    
#     def brutto(self):
#         return self.__price + (self.__price + self.__vat) / 100
    
# dysk1 = disc("IBM", 2048, 330.0)
# print(dysk1.info())



# class osoba:
#     def __init__(self,imie,nazwisko,wiek):
#         self.imie = imie
#         self.nazwisko = nazwisko
#         self.wiek = wiek
#     def pokaz(self):
#         print(f"imie: {self.imie} nazwisko: {self.nazwisko} wiek: {self.wiek}")
# class student(osoba):
#     kierunek = ''
#     def __init__(self,imie,nazwisko,wiek,kierunek):
#         self.imie = imie
#         self.nazwisko = nazwisko
#         self.wiek = wiek
#         self.kierunek = kierunek
#     def pokaz(self):
#         print(f"imie: {self.imie} nazwisko: {self.nazwisko} wiek: {self.wiek} kierunek: {self.kierunek}")
# o1 = osoba('jan','kowalski',15)
# s1 = student('ania','nowak',45,'informatyka')
# s1.kierunek = 'informatyka'
# s1.pokaz()
# o1.pokaz()



# class Uczen:
#     def __init__(self,imie,nazwisko,pesel):
#         self.imie = imie
#         self.nazwisko = nazwisko
#         self.pesel = pesel
#     def ustaw_imie(self):
#         imie = input('imie? ')
#     def ustaw_nazwisko(self):
#         nazwisko = input('nazwisko? ')
#     def ustaw_pesel(self):
#         pesel = input('pesel? ')
# class UczenSzkoly(Uczen):
#     def __init__(self,imie,nazwisko,pesel,nazwa_szkoly):
#         super().__init__(imie, nazwisko, pesel)
#         self.nazwa_szkoly = nazwa_szkoly
#     def ustaw_szkole(self):
#         nazwa_szkoly = input('Nazwa szkoly? ')
#     def pokaz(self):
#         print(f"imie: {self.imie} nazwisko: {self.nazwisko} wiek: {self.pesel} kierunek: {self.nazwa_szkoly}")

# s1 = UczenSzkoly('Jan','Kowal','01222222','Technikum')
# s1.pokaz()



# class osoba:
#     def __init__(self,imie,plec):
#         self.imie = imie
#         self.plec = plec
#     def wyswietlInfo(self):
#         print(f"imie: {self.imie} plec: {self.plec}")
# class student(osoba):
#     def __init__(self, imie, plec, punktacja):
#         super().__init__(imie, plec)
#         self.punktacja = punktacja
#     def wyswietlInfo(self):
#         print(f"imie: {self.imie} plec: {self.plec} punktacja: {self.punktacja}")
# o1=osoba('Tomasz', 'Mezczyzna')
# o1.wyswietlInfo()
# s1=student('Tomasz', 'Mezczyzna', 10)
# s1.wyswietlInfo()

# class szkola():
#     def __init__(self):
#         self.lista = []
#     def dodaj_ucznia(self,obiekt_uczen):
#         self.lista.append(obiekt_uczen)
#     def pokaz_uczniow(self):
#         for i in range(len(self.lista)):
#             self.lista[i].pokaz()
#         for i in self.lista:
#             i.pokaz()
# uczen1 = uczen("jan","kowalski",18)
# uczen2 = uczen("anna","kowalska",19)
# szkola1 = szkola()
# szkola1.dodaj_ucznia(uczen1)
# szkola1.dodaj_ucznia(uczen2)
# szkola1.pokaz_uczniow()


class zamowienie:
    def __init__(self, maksrozmiar):
        self.pozycje = []
        self.iledodanych = 0
        self.maksrozmiar = maksrozmiar
    def dodajpozycje(self,pozycja):
        if self.iledodanych < self.maksrozmiar:
               self.pozycje.append(pozycja)
               self.iledodanych += 1
        else:
            print("nie ma miejsca")
    def oblicz(self):
        suma = 0
        for i in self.pozycje:
            suma += i.oblicz()
        return suma
    def tostring(self):
        print(f"Razem {self.oblicz()} zł")
class pozycja:
    def __init__(self,nazwa_towaru, ile_sztuk, cena):
        self.nazwa_towaru = nazwa_towaru
        self.ile_sztuk = ile_sztuk
        self.cena = cena
    def oblicz(self):
        return self.ile_sztuk * self.cena
    def tostring(self):
        print(f"{self.nazwa_towaru} {self.cena} zł {self.ile_sztuk} szt {self.oblicz()} zł")
    
z = zamowienie(10)
z.dodajpozycje(pozycja("chleb",1,3.5))
z.dodajpozycje(pozycja("cukier",3,4.0))
z.tostring()