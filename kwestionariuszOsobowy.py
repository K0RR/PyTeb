from tkinter import *
from tkinter import filedialog, ttk

window = Tk()
lista = []
wybor = IntVar()
imie = Entry(window)
imie.grid(row=0, column=0)

nazwisko = Entry(window)
nazwisko.grid(row=1, column=0)

e_imie = Label(text="Imie").grid(row=0, column=2)
e_nazwisko = Label(text="Nazwisko").grid(row=1, column=2)

wiek_opcje = list(range(1, 100))
wiek = ttk.Combobox(window, values=wiek_opcje)
wiek.grid(row=2, column=0)
e_wiek = Label(text="Wiek").grid(row=2, column=2)

plec_k = Radiobutton(text='K', variable=wybor, value=1)
plec_k.grid(row=3, column=0)

plec_m = Radiobutton(text='M', variable=wybor, value=0)
plec_m.grid(row=3, column=1)


def pobierz_woj():
    plik = open('woj.txt', "r")
    woj = [line.strip() for line in plik]
    plik.close()
    drop = ttk.Combobox(window, values=woj)
    drop.grid(row=4, column=0)
    return drop


drop = pobierz_woj()


def zapisz():
    lista.append(imie.get())
    lista.append(nazwisko.get())
    lista.append(wiek.get() + " lat(a)")

    if wybor.get() == 0:
        plec = "mezczyzna"
    else:
        plec = "kobieta"

    lista.append(plec)
    lista.append(drop.get())
    nazwa_pliku = filedialog.asksaveasfilename(filetypes=[("Dowolny plik", "*.txt")])
    plik = open(nazwa_pliku, "w")
    plik.write(', '.join(map(str, lista)))
    plik.close()


zapis = Button(text="zapisz do pliku", command=zapisz)
zapis.grid(row=4, column=2)

window.mainloop()
