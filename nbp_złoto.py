import requests
from tkinter import *

okno = Tk()
okno.geometry('220x120')


def licz():
    global data_max, data_min, z_min, z_max
    licznik = 0
    response = requests.get("http://api.nbp.pl/api/cenyzlota/last/" + str(wprowadz_dni.get())).json()
    for element in response:
        if licznik == 0:
            z_min = float(element['cena'])
            z_max = float(element['cena'])
            licznik = 1
        if z_min >= float(element['cena']):
            z_min = element['cena']
            data_min = element['data'] + ' - ' + str(z_min)
        if z_max <= float(element['cena']):
            z_max = element['cena']
            data_max = element['data'] + ' - ' + str(z_max)

    napis_max = Label(okno, text='max: ' + data_max, padx=5)
    napis_max.grid(row=2, column=0, columnspan=2)
    napis_min = Label(okno, text='min: ' + data_min, padx=5)
    napis_min.grid(row=3, column=0, columnspan=2)


wprowadz_dni = Entry(okno, width=15)
wprowadz_dni.grid(row=0, column=0)
ile_dni = Button(okno, text='Ile dni', padx=5, command=licz)
ile_dni.grid(row=0, column=1)

okno.mainloop()
