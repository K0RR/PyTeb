from tkinter import *
from tkinter import filedialog

window = Tk()


def wczytaj():
    plik = open('prod.txt', "r")
    linia = [line.strip() for line in plik]
    plik.close()
    lista_w = Listbox()
    for i in linia:
        lista_w.insert(END, i)
    lista_w.grid(row=1, column=0)
    return lista_w


def zapisz():
    lista_z = Listbox()
    lista_z.grid(row=1, column=2)
    plik = open('prod.txt', "w")
    plik.write(lista_z.get())
    plik.close()
    return lista_z


def dodaj():
    lista_z.insert(END, b_lewo.get())


def usun():
    lista_z.remove(b_prawo.get())


b_wczytaj = Button(text="Wczytaj", command=wczytaj).grid(row=0, column=0)
b_lewo = Button(text="<", command=dodaj).grid(row=1, column=1)
b_prawo = Button(text=">", command=usun).grid(row=2, column=1)
b_zapisz = Button(text="Zapisz", command=zapisz).grid(row=0, column=2)

window.mainloop()