import PySimpleGUI as sg  # Part 1 - The import

# Define the window's contents
layout = [[sg.Text("What's your name?")],  # Part 2 - The Layout
          [sg.Input()],
          [sg.Button('Ok')]]

# Create the window
window = sg.Window('Window Title', layout)  # Part 3 - Window Definition

# Display and interact with the Window
event, values = window.read()  # Part 4 - Event loop or Window.read call

# Do something with the information gathered
print('Hello', values[0], end="")
print("! Thanks for trying PySimpleGUI")

# Finish up by removing from the screen
window.close()  # Part 5 - Close the Window




ncols = "g"
print(f"\033[F\033[appended text")

print('\033[36mZWYKLY\033[96m JASNY\033[0m')