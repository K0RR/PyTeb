import mysql.connector

# mydb = mysql.connector.connect(host="localhost",user="root",password="",database="python")
# mycursor = mydb.cursor()
# mycursor.execute("SELECT * FROM uczniowie")
# myresult = mycursor.fetchall()
# imie = input("imie? ")
# licznik = 0
# for i in myresult:
#     if i[1] == imie:
#         licznik += 1
# if licznik == 1:
#     print(imie, 'pojawia sie jeden raz')
# else:
#     print(imie, 'pojawia sie', licznik, 'razy')


# print("Co chcesz wyswietlic: \n 1. imie \n 2. nazwisko \n 3. wiek")
# co = int(input('      : '))
# print("Jak to wyswietlic: \n 1. od a do z \n 2. od z do a \n 3. domysl sie")
# jak = int(input('      : '))
# if co == 1:
#     wybor1 = 'imie'
# if co == 2:
#     wybor1 = 'nazwisko'
# if co == 3:
#     wybor1 = 'wiek'
# if jak == 1:
#     wybor2 = 'ORDER BY ' + wybor1
# if jak == 2:
#     wybor2 = 'ORDER BY ' + wybor1 + ' DESC'
# if jak == 3:
#     wybor2 = ''
# mycursor.execute(f"SELECT {wybor1} FROM uczniowie {wybor2}")
# myresult = mycursor.fetchall()
# for i in myresult:
#     print(i[0])



# mydb = mysql.connector.connect(host="localhost",user="root",password="",database="employee")
# mycursor = mydb.cursor()

# kraj = input('kraj? ')
# ile = 0
# mycursor.execute(f"SELECT JOB_COUNTRY FROM employee")
# myresult = mycursor.fetchall()
# for i in myresult:
#     if i[0] == kraj:
#         ile += 1
# if ile > 0:
#     print(ile)
# else:
#     print('nie ma takiego pracownika')


# mycursor.execute(f"SELECT FIRST_NAME, YEAR(HIRE_DATE) FROM employee")
# myresult = mycursor.fetchall()
# for i in myresult:
    # print(str(i[0]) + ' - ' + str(int(2023) - int(i[1])))
    # bez funcji YEAR można tak:
    # rok = str(i[1])
    # print(str(i[0]) + ' - ' + str(2023 - int(rok[0:4])))


# mycursor.execute("SELECT JOB_COUNTRY, SALARY FROM employee")
# myresult = mycursor.fetchall()
# salary = 0.0
# sum = 0
# ile_typa = 0
# for i in myresult:
#     ile_typa += 1
#     if i[0] == 'USA':
#         salary = float(i[1]) * 1.2
#         sum += salary
#     if i[0] == 'Canada':
#         salary = float(i[1]) * 1.1
#         sum += salary
#     if i[0] == 'England':
#         salary = float(i[1]) * 1.05
#         sum += salary
#     else:
#         salary = float(i[1]) * 1.02
#         sum += salary
# srednia = sum/ile_typa
# print(srednia)


# mycursor.execute("SELECT * FROM country")
# myresult = mycursor.fetchall()
# plik = open('E:/file.csv', 'w')
# for i in myresult:
#     plik.write(str(i[0]) + ' - ' + str(i[1]) + '\n')
# plik.close



# import baza
# wybor = ''
# while wybor != 0:
#     wybor = int(input('\n\n0. wyjdz\n1. pokaz ucznia\n2. edytuj ucznia\n3. usun ucznia\n4. dodaj ucznia\n\n'))
#     if wybor == 1:
#         baza.pokaz_ucznia()
#     if wybor == 2:
#         baza.edytuj_ucznia()
#     if wybor == 3:
#         baza.usun_ucznia()
#     if wybor == 4:
#         baza.dodaj_ucznia()



# plik = open('C:\\Users\\user\\Downloads\\uczniowie.txt', 'r',encoding='utf-8')
# mydb = mysql.connector.connect(host="localhost",user="root",password="",database="uczniowie")
# mycursor = mydb.cursor()
# for i in plik:
#     lines = i.strip().split(' ')
#     nazwisko = lines[1]
#     imie = lines[2]
#     wiek = lines[3]
#     mycursor.execute(f"INSERT INTO uczniowie (nazwisko,imie,wiek) VALUES ('{nazwisko}','{imie}','{wiek}')")
# mydb.commit()
# mydb.close()
# plik.close


mydb = mysql.connector.connect(host="localhost",user="root",password="",database="uczniowie")
mycursor = mydb.cursor()
mycursor.execute("SELECT wiek, id_ucznia FROM uczniowie")
myresult = mycursor.fetchall()
for i in myresult:
                #   STARA WERSJA NIESKOŃCZONA
                    # wiek = str(i[0])
                    # if int(wiek) > 30:
                    #     nowy_wiek = int(wiek[0]) + int(wiek[1])
                    #     print(nowy_wiek[0])
    if i[0] >= 30:
        nowy_wiek = i[0]%10 + (i[0]//10)%10 + 10
        mycursor.execute(f"UPDATE uczniowie SET wiek = {nowy_wiek} WHERE id_ucznia = '{i[1]}'")
        mydb.commit()
mydb.close()