import requests
from tkinter import *

url = ['https://api.nbp.pl/api/exchangerates/rates/a/usd', 'https://api.nbp.pl/api/exchangerates/rates/a/gbp', 'https://api.nbp.pl/api/exchangerates/rates/a/eur']
curr = ['usd', 'gbp', 'eur']
okno = Tk()
okno.geometry('220x120')


def oblicz():
    # global usd, gbp, eur
    # x = -1
    # for z in url:
    #     response = requests.get(z)        # TODO to nie jest skończone i poprawne, poniżej stara wersja, która działa
    #     dane = response.json()
    #     x += 1
    #     print(curr[x])
    #     for element in dane:
    #         curr[x] = dane['rates'][0]['mid']

    response = requests.get(url[0])
    dane = response.json()
    for element in dane:
        usd = dane['rates'][0]['mid']

    response = requests.get(url[1])
    dane = response.json()
    for element in dane:
        gbp = dane['rates'][0]['mid']

    response = requests.get(url[2])
    dane = response.json()
    for element in dane:
        eur = dane['rates'][0]['mid']

    usd, gbp, eur = float(wprowadz_cena.get()) / usd, float(wprowadz_cena.get()) / gbp, float(wprowadz_cena.get()) / eur
    napis_usd = Label(okno, text=str(round(usd, 2)) + ' usd', padx=5)
    napis_usd.grid(row=2, column=0)
    napis_gbp = Label(okno, text=str(round(gbp, 2)) + ' gbp', padx=5)
    napis_gbp.grid(row=3, column=0)
    napis_eur = Label(okno, text=str(round(eur, 2)) + ' eur', padx=5)
    napis_eur.grid(row=4, column=0)


wprowadz_cena = Entry(okno)
wprowadz_cena.grid(row=0, column=0)
napis = Label(okno, text='zł', padx=5)
napis.grid(row=0, column=1)
przycisk = Button(text='Oblicz', command=oblicz)
przycisk.grid(row=1, column=0, columnspan=2)

okno.mainloop()
