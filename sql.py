import mysql.connector
import sql_
from tkinter import *

okno = Tk()
okno.geometry('220x90')

napis_login = Label(okno, text='Login:', padx=5)
napis_login.grid(row=0, column=0)
wprowadz_login = Entry(okno)
wprowadz_login.grid(row=0, column=1)
napis_haslo = Label(okno, text='Hasło:', padx=5)
napis_haslo.grid(row=1, column=0)
wprowadz_haslo = Entry(okno)
wprowadz_haslo.grid(row=1, column=1)

def zaloguj():
    login = wprowadz_login.get()
    haslo = wprowadz_haslo.get()
    try:
        global mydb
        mydb = mysql.connector.connect(host="localhost", user=login, password=haslo, database="python")
        okno.destroy()
        sql_.menu(mydb)
    except:
        komunikat = Label(text='Niezalogowano')
        komunikat.grid(row=3, column=0)

przycisk_zaloguj = Button(text='Zaloguj', command=zaloguj)
przycisk_zaloguj.grid(row=2, column=0, columnspan=2)

okno.mainloop()
mydb.close()
