from tkinter import *
import sql_mod


def menu(mydb):  # Dodajemy parametr mydb
    okno_menu = Tk()
    okno_menu.geometry('200x200')
    napis = Label(okno_menu, text="Menu")
    napis.grid(row=0, column=0)
    wprowadz = Button(okno_menu, text="WPROWADŹ", command=lambda: sql_mod.dodaj(mydb))
    wprowadz.grid(row=1, column=0)
    wyszukaj = Button(okno_menu, text="WYSZUKAJ", command=lambda: sql_mod.szukaj(mydb))
    wyszukaj.grid(row=2, column=0)
    usun = Button(okno_menu, text="USUŃ", command=lambda: sql_mod.usun(mydb))
    usun.grid(row=3, column=0)
