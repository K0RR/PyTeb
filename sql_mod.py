import mysql.connector
from tkinter import *


def wyszukaj():
    nr = wprowadz_nr.get()
    mycursor.execute(f"SELECT * FROM users WHERE id = {nr}")
    myresult = mycursor.fetchall()
    komunikat = Label(okno_mod)
    komunikat.grid(row=3, column=0)
    if myresult == []:
        komunikat.config(text='Nie ma takiego ID')
    else:
        # komunikat.delete('1.0', END) # TODO wyczysc label bo tekst sie naklada
        komunikat.config(text='LOGIN: ' + str(myresult[0][1]) + '; HASŁO: ' + str(myresult[0][2]) + '; EMAIL: ' + str(myresult[0][3]) + '; TYP: ' + str(myresult[0][4]))


def szukaj(mydb):
    global okno_mod, mycursor, wprowadz_nr
    okno_mod = Tk()
    okno_mod.geometry('400x200')
    mycursor = mydb.cursor()
    wprowadz_nr = Entry(okno_mod)
    wprowadz_nr.grid(row=0, column=0)
    przycisk = Button(okno_mod, text="Zatwierdź", command=wyszukaj)
    przycisk.grid(row=2, column=0)
    komunikat_id = Label(okno_mod, text='Wprowadź ID')
    komunikat_id.grid(row=1, column=0)


def nowy_user(mydb):
    global mycursor, wprowadz_imie, wprowadz_haslo, wprowadz_email, wprowadz_typ
    mycursor = mydb.cursor()
    imie, haslo, email, typ = wprowadz_imie.get(), wprowadz_haslo.get(), wprowadz_email.get(), wprowadz_typ.get()
    mycursor.execute(f"INSERT INTO users (login,password,e_mail,typ) VALUES ('{imie}','{haslo}','{email}','{typ}')")
    mydb.commit()


def dodaj(mydb):
    global okno_mod, mycursor, wprowadz_imie, wprowadz_haslo, wprowadz_email, wprowadz_typ
    okno_mod = Tk()
    okno_mod.geometry('400x200')
    mycursor = mydb.cursor()

    wprowadz_imie = Entry(okno_mod)
    wprowadz_imie.grid(row=0, column=0)
    komunikat_imie = Label(okno_mod, text='Wprowadź imie')
    komunikat_imie.grid(row=0, column=1)
    wprowadz_haslo = Entry(okno_mod)
    wprowadz_haslo.grid(row=1, column=0)
    komunikat_haslo = Label(okno_mod, text='Wprowadź haslo')
    komunikat_haslo.grid(row=1, column=1)
    wprowadz_email = Entry(okno_mod)
    wprowadz_email.grid(row=2, column=0)
    komunikat_email = Label(okno_mod, text='Wprowadź email')
    komunikat_email.grid(row=2, column=1)
    wprowadz_typ = Entry(okno_mod)
    wprowadz_typ.grid(row=3, column=0)
    komunikat_typ = Label(okno_mod, text='Wprowadź typ konta')
    komunikat_typ.grid(row=3, column=1)

    przycisk = Button(okno_mod, text="Zatwierdź", command=lambda: nowy_user(mydb))
    przycisk.grid(row=4, column=0)


# def usun():
#     mycursor = mydb.cursor()
#
#     nr_ucznia = input('nr ucznia: ')
#     mycursor.execute(f"SELECT * FROM uczniowie WHERE id_ucznia = {nr_ucznia}")
#     myresult = mycursor.fetchall()
#     if myresult == []:
#         print("nie ma takiego ucznia")
#     else:
#         mycursor.execute(f"DELETE FROM uczniowie WHERE id_ucznia = '{nr_ucznia}'")
#     mydb.commit()