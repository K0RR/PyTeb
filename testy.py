def wczytaj_plik_do_slownika(nazwa_pliku):
    slownik = {}
    with open(nazwa_pliku, 'r') as plik:
        for linia in plik:
            linia = linia.strip()  # Usunięcie białych znaków z początku i końca linii
            if linia:
                login, haslo = linia.split(':')
                slownik[login] = haslo
    return slownik

# Przykład użycia
nazwa_pliku = 'loginy.txt'
slownik_loginow = wczytaj_plik_do_slownika(nazwa_pliku)
print(slownik_loginow)
